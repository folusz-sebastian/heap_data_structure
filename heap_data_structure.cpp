#include<iostream>
#include <queue>
#include <stack>

using namespace std;
class Element
{
public:
    Element* lchild;
    Element* rchild;
    int data;
    Element(int d)
    {
        lchild=NULL;
        rchild=NULL;
        data=d;
    }
};

Element* root = NULL;
int razy=0;


class Heap{
public:
    void remove(Element *root);
    void insert(Element* &root,int d,int i,int n);
    void display (Element *root);
    void restore (Element *root);
    void sort (Element *root, int tab [], int size);
    Heap (){
        root = NULL;
    }

};

void Heap::remove(Element *root)
{
    int x;
    int prNull;
    int licznik = 0;
    int i = 1;
    int removerazy = razy;
    std::stack < int > p;
    while (removerazy != 1) {
        licznik++;
        x = (removerazy % 2);
        p.push(x);
        while (i>0){
            prNull = x;
            i--;
        }
        removerazy = (removerazy / 2);
    }
    Element* tmp = root;
    Element* sectmp;

    while ((licznik-1)>0){
        licznik--;
        if (p.top()==1){
            tmp = tmp->rchild;
        }else{
            tmp = tmp->lchild;
        }
        p.pop();
    }

    if (p.top()==1){
        sectmp = tmp->rchild;
    }else{
        sectmp = tmp->lchild;
    }
    p.pop();

    int tmp2=root->data;		//zamiana korzenia z ostatnim elementem
    root->data=sectmp->data;       //tmp2 to wartosc najmniejszego elementu - z korzenia
    sectmp->data=tmp2;
    delete (sectmp);
    if (prNull == 1){
        tmp->rchild = NULL;
    }else{
        tmp->lchild = NULL;
    }

    razy--;
}

void Heap::insert(Element* &root,int d,int i,int n)
{
    if(i>n){
        return;

    }
    if(i==n)
    {
        razy = (n+1);
        root=new Element(d);
        return;
    }
    insert(root->lchild,d,(2*i)+1,n);
    insert(root->rchild,d,(2*i)+2,n);

    if((root->lchild)&&(root->rchild))
    {
        if(root->data > root->lchild->data)
        {
            int temp=root->lchild->data;		//zamiana jesli wiekszy korzen od lewego
            root->lchild->data=root->data;
            root->data=temp;
        }
        if(root->data > root->rchild->data)		//zamiana jesli wiekszy korzen od prawego
        {
            int temp=root->rchild->data;
            root->rchild->data=root->data;
            root->data=temp;
        }
    }

    if((root->lchild)&&(!root->rchild))
    {
        if(root->data > root->lchild->data)     //zamiana jesli wiekszy korzen od lewego
        {
            int temp=root->lchild->data;
            root->lchild->data=root->data;
            root->data=temp;
        }
    }

    if((!root->lchild)&&(root->rchild))     //zamiana jesli wiekszy korzen od prawego
    {
        if(root->data > root->rchild->data)
        {
            int temp=root->rchild->data;
            root->rchild->data=root->data;
            root->data=temp;
        }
    };

}

void Heap::display (Element *root){
    std::queue<Element *> q;
    q.push(root);
    int j=15;
    int z=0;
    int k = 4;
    int l;
    while(q.size() > 0){
        int levelNodes = q.size();
        int i = j;
        while(levelNodes > 0){
            l=k;
            Element *p = q.front();
            q.pop();
            while (i>0){
                cout << " ";
                i--;
            }
            cout << p->data;
            while (l > 0) {
                cout<< " ";
                l--;
            }
            if(p->lchild != NULL) q.push(p->lchild);
            if(p->rchild != NULL) q.push(p->rchild);
            levelNodes--;
        }
        cout << endl;
        z++;
        switch (z){         //statyczne nadanie dlugosci odstepow pomiedzy wierzcholkami
            case 1:
                j=11;
                k=7;
                break;
            case 2:
                j=9;
                k=3;
                break;
            case 3:
                j=8;
                k=1;
                break;
        }

    }
}

void Heap::restore(Element *root) {
    Element* tmp3 = root;
    int zmienna;
    while ((tmp3->lchild) || (tmp3->rchild)){
        if((tmp3->lchild)&&(tmp3->rchild))
        {
            if(tmp3->rchild->data > tmp3->lchild->data)
            {
                if (tmp3->lchild->data < tmp3->data){
                    zmienna = tmp3->data;
                    tmp3->data=tmp3->lchild->data;
                    tmp3->lchild->data = zmienna;
                    tmp3 = tmp3->lchild;
                }
                else {
                    return ;
                }
            }
            else {
                if (tmp3->rchild->data < tmp3->data){
                    zmienna = tmp3->data;
                    tmp3->data=tmp3->rchild->data;
                    tmp3->rchild->data = zmienna;
                    tmp3 = tmp3->rchild;
                }else{
                    return ;
                }
            }
        }

        else if((tmp3->lchild)&&(!tmp3->rchild))
        {
            if (tmp3->lchild->data < tmp3->data){
                zmienna = tmp3->data;
                tmp3->data=tmp3->lchild->data;
                tmp3->lchild->data = zmienna;
            }
            tmp3 = tmp3->lchild;

        }

        else if((!tmp3->lchild)&&(tmp3->rchild))
        {
            if (tmp3->rchild->data < tmp3->data){
                zmienna = tmp3->data;
                tmp3->data=tmp3->rchild->data;
                tmp3->rchild->data = zmienna;
            }
            tmp3 = tmp3->rchild;
        }
    }
}

void Heap::sort(Element *root, int tab[], int size) {
    int d;
    int n=0;
    int i=0;
    int k = 0;
    int size2 = size;
    while(size != 0)
    {
        d=tab[k];
        insert(root,d,0,n);
        n++;
        k++;
        size--;
    }
    while(size2 > 1)
    {
        tab[i] = root->data;
        remove(root);
        restore(root);
        i++;
        size2--;
    }
    tab[i] = root->data;
}

int main()
{
    //dodanie elementow do kopca
    Heap* heap = new Heap;
    heap->insert(root, 3, 0, 0);
    heap->insert(root, 5, 0, 1);
    heap->insert(root, 1, 0, 2);
    heap->insert(root, 7, 0, 3);
    heap->insert(root, 9, 0, 4);
    heap->insert(root, 2, 0, 5);
    heap->insert(root, 8, 0, 6);
    heap->insert(root, 4, 0, 7);

    //wyswietlenie struktury kopca
    heap->display(root);
    cout<<endl;
    //usuniecie elementu
    heap->remove(root);
    //przywrocenie wlasnosci kopca
    heap->restore(root);
    //ponowne wyswietlenie struktury kopca
    heap->display(root);
    //stworzenie (size)-elementowej tablicy z liczbami
    int size = 8;
    int tablica [size] = {2, 9, 1, 3, 5, 7, 6, 4};

    //sortowanie przez kopcowanie
    heap->sort(root, tablica, size);
    //wyswietlenie posortowanych elementow
    for (int i = 0; i < 8; ++i) {
        cout<<tablica[i];
    }

    return 0;
}
